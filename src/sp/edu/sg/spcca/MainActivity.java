package sp.edu.sg.spcca;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import org.apache.cordova.*;

public class MainActivity extends DroidGap {

	
	private static final String TAG = null;
	private static Context context;

	private static final String LOG_TAG = null;
  	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
	    MainActivity.context =  getApplicationContext();
        // Set by <content src="index.html" /> in config.xml
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 	
 	    super.setIntegerProperty("splashscreen", R.drawable.splash);
 		super.loadUrl(Config.getStartUrl(), 2000);
     //   super.loadUrl(Config.getStartUrl());
        //super.loadUrl("file:///android_asset/www/index.html")
	}
    
    public static Context getAppContext() {
        return MainActivity.context;
     }

	
	


}
