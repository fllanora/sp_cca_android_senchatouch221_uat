/*****************************************************
 * WebSSO Library
 *
 * CONTROLLER ARRAY
 *
 *****************************************************/
var AUTHENTICATION_STAFFONLY = false;
var AUTHENTICATION_STUDENT_AND_STAFF = true;
var AUTHENTICATION_STUDENTONLY = false;
var OFFLINESUPPORT = true;

function WebSSOControllerArray()
{
    
    Ext.dispatch({
                 controller: app.controllers.WebSSOController,
                 action: 'logout'
                 });
    
    SP.WebSSOPlugin.clearUserData();
	
    var webSSOLogoutControllerArray;

    
    try{
        webSSOLogoutControllerArray[0] = app.controllers.myCPController;
    //    webSSOLogoutControllerArray[1] = app.controllers.viewbookingticketscontroller;
    //    webSSOLogoutControllerArray[2] = app.controllers.viewmybookingcontroller;
     //   webSSOLogoutControllerArray[3] = app.controllers.viewcollectionformcontroller

        
        if((webSSOLogoutControllerArray!=null) && (webSSOLogoutControllerArray.length>0))
        {
            for(var i=0; i<webSSOLogoutControllerArray.length;i++)
            {
                Ext.dispatch({
                             controller: webSSOLogoutControllerArray[i],
                             action: 'WebSSOLogout'
                             });    
            }
        }
   }
    catch(err){}

}