Ext.define('app.view.ccaComponentsView', {
    extend: 'Ext.Panel',
    xtype: 'ccaComponentsView',
    
   requires: [
       'Ext.TitleBar',
        'app.view.ccaComponentsList'
   ],
    
    config: {
        title: 'Components',
        iconCls: 'more',
	    style: 'background-color:#FFFFFF',
        layout: 'fit',

        items:[{
            xtype: 'titlebar',
            title: 'Components',
               docked: 'top',
               items: [
                        {
                        xtype: 'button',
                        text: 'Back',
                        ui: 'back',
                        handler: function() {
                       try
                       {
                       el_info_button01_counter = 0;
                       el_info_button02_counter = 0;
                       el_info_button03_counter = 0;
                       el_info_button04_counter = 0;
                       el_info_button05_counter = 0;
                       el_info_button06_counter = 0;
                       el_info_button07_counter = 0;
                       el_info_button08_counter = 0;
                       }
                       catch(err){}
                       
                            Ext.getCmp('ccaScoresCardView').animateActiveItem(0,{type: 'slide', direction: 'right'});
                       resetBottomContainer(ccaGrade);
                       
                       }}]

               }, {xtype: 'ccaComponentsList', id:'ccaComponentsList', name: 'ccaComponentsList'}]
    }
});

