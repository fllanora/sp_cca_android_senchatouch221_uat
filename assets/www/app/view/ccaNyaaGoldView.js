Ext.define('app.view.ccaNyaaGoldView', {
    extend: 'Ext.Panel',
    xtype: 'ccaNyaaGoldView',
    
 
	requires: [
       'Ext.TitleBar',
		'app.view.ccaNyaaGoldCarousel',
   ],
    
    config: {
        title: 'NYAA Gold',
        iconCls: 'more',
   	    style: 'background-color:#000000',
        layout: 'vbox',
			scrollable: null,
           
           
        listeners: {
            show: function(){
           processNYAALink();
           
           }
           },
           
        items:[
			{
            xtype: 'titlebar',
            title: 'NYAA Gold',
               docked: 'top',  items: [
                                      {
                                      iconCls: 'settings',
                                      ui: 'plain',
                                      iconMask: true,
                                      align: 'right',
                                      
                                      handler: function() {
                                         displaySettingsPanel(this);
                                      }}]

        }, 
			{
            layout: 'fit',
            xtype: 'ccaNyaaGoldCarousel',
            flex: 7
	
        },
			{
            xtype: 'panel',
            style: 'background-color:#000000',
				html: "<span style='font-size:0.7em'><font color='#969696'><center>For more details, visit </font></span><a id='nyaalinktap'><font color='#969696' size=2><b><u>www.nyaa.org</u></b></a></center></font>",
				 layout: 'fit',
				flex: 1
            
        }
		
		
		]

    }
});


