Ext.define('app.view.mainView', {
    extend: 'Ext.tab.Panel',
    xtype: 'mainView',

    requires: [
       'app.view.ccaScoresCardView',
        'app.view.ccaGroupsView',
         'app.view.ccaNyaaGoldView',
         'app.view.ccaAboutView'
   ],
    config: {
         tabBar: {
            docked: 'bottom'
         },
           listeners: {
                activeitemchange: function (tabPanel, tab, oldTab) {
                console.log("Tab: "+tab.config.title);
                if(tab.config.title=="CCA Scores")
                {
                  animFlag = 'current';
                btnFlag = 'current';
                Ext.getCmp('ccaScoresCardView').setActiveItem(0);
                Ext.getCmp('scoreSegmentedButton').setPressedButtons([0]);
                Ext.getCmp('bottomScores').setActiveItem(0); 
                   resetBottomContainer(ccaGrade);
                }else
                {
                deActivateAnim();
                }
           
             }
            }, 
        items: [
               {
           // iconCls: 'iconScore',
                iconCls:  'ccaScores',
                	//iconMask : true,
            xtype: 'ccaScoresCardView'
        },
         {
			iconCls: 'team',
            xtype: 'ccaGroupsView'
        },
               {
			iconCls: 'star',
            xtype: 'ccaNyaaGoldView'
        },
                {
                
                iconCls: 'ccaFeeds',
                xtype: 'ccaFeedsView'
                },
   			{
               iconCls: 'info',
            xtype: 'ccaAboutView'
        }]
     

    }
     
});
