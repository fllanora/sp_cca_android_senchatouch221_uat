Ext.define('app.view.ccaAboutView', {
    extend: 'Ext.Panel',
    xtype: 'ccaAboutView',
    
   requires: [
       'Ext.TitleBar'
   ],
    
    config: {
        title: 'About',
       //   iconCls: 'info',
        scrollable: 'vertical',
        styleHtmlContent: true,
	    style: 'background-color:#FFFFFF',

        items:[{
            xtype: 'titlebar',
            title: 'About',
            docked: 'top',
               items: [
                       {
                       iconCls: 'settings',
                       ui: 'plain',
                       iconMask: true,
                       align: 'right',
                       
                       handler: function() {
                       displaySettingsPanel(this);
                       }}]

        }, {
            html: [
               '<br/><center><img width="200px" src="app/view/images/sp_logo.png" /></center><div style="margin-left:10px; max-width:95%;text-align:left;font-size:14px;font-family:arial;"><br><b>About CCA</b><br>The Co-Curricular Activities (CCA) is an integral part of our education system to drive home the message that holistic development goes beyond good academic grades. The current technological revolution, global competition, and push towards a knowledge-based economy require EQ, AQ and soft skills like creativity, leadership abilities, teamwork, flexibility, communication skills, resilience and an enterprising spirit. <br/><br/>SP CCA is a Singapore Polytechnic (SP) mobile application. Established in 1954, SP is Singapore\'s first polytechnic. Its ten academic schools with a student population of 16,000 offer 50 full-time diploma courses. Among SP\'s 160,000 graduates are successful entrepreneurs, top executives in multinational and public-listed corporations, and well-known professionals. <br/><br/><br><b>Acknowledgements:</b><br><br><b>Staff</b><br>Ng Kwee Chek, DSD<br>Ramana Rao K V, INDT<br>Ryan Sena, INDT<br>Ng Meng Siew, INDT<br>Sean Goh, CCOM<br>With contribution of other staff from INDT and DSD.<br>Email: <a href="mailto:servicedeskmail@sp.edu.sg">servicedeskmail@sp.edu.sg</a><br>Website: <a href="http://www.sp.edu.sg/">http://www.sp.edu.sg/</a><br><a href="https://play.google.com/store/apps/developer?id=Singapore+Polytechnic">Our Other Apps</a><br><br></div>'
            ].join('')
        }]

    }
});

