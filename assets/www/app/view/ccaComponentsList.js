Ext.define('app.view.ccaComponentsList', {
    extend: 'Ext.List',

    xtype: 'ccaComponentsList',
    
    requires: [
        'app.store.ccaComponentsStore'
    ],

    config: {
              cls: 'customHeader',
            emptyText: 'No data found!',
           itemId: 'ccaComponentsList',
           id: 'ccaComponentsList',
           title: 'CCA Components',
         
        emptyText: 'No data found!',
        store: 'ccaComponentsStore',
           
           grouped : true,
           indexBar: true,
           itemTpl : new Ext.XTemplate(
                                       '<div style="float:left; margin-left:10px;width:200px;">',
                                       '{activity}<br/>',
                                       '<span style="font-size:14px;">({rolename})<br/></span>',
                                       '<span style="font-size:12px;">Date: {start_date} - {end_date}</span>',
                                       '</div>',
                                       '<div style="float:left; margin-left:10px;width:50px; font-size:15px;">',
                                       '{points_activity} Pts',
                                       '</div><br><br><br><br><br>'
                                       ),
	}

});

