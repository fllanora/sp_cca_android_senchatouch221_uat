Ext.define('app.view.ccaScoresView', {
    extend: 'Ext.Panel',
    xtype: 'ccaScoresView',
    
   requires: [
       'Ext.TitleBar'
   ],
           
    
    config: {
        itemId: 'ccaScoresView',
        title: 'CCA Scores',
      
       style: 'background-color:#000000;',
        layout: {
        type: 'vbox',
        align: 'stretch', pack: 'top'},
        
           
        items:[{
            xtype: 'titlebar',
            title: 'CCA Scores',
               docked: 'top',  items: [
                                       {
                                       iconCls: 'settings',
                                       ui: 'plain',
                                       iconMask: true,
                                       align: 'right',
                                        
                                       handler: function() {
                                       displaySettingsPanel(this);
                                       
                                       }}]

               },{xtype: 'panel', id: 'topScores'}, {xtype: 'panel', flex: 5,layout: {type: 'hbox', align: 'stretch', pack: 'center'},items:[{xtype: 'spacer'},{xtype: 'carousel', id: 'bottomScores'},{xtype: 'spacer'}]}]
        
    }
             
});

