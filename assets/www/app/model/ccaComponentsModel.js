Ext.define('app.model.ccaComponentsModel', {
           extend: 'Ext.data.Model',
           
           config: {
           
           fields: [
                    {name: 'activity', type: 'string'},
                    {name: 'rolename',  type: 'string'},
                    {name: 'start_date',       type: 'string'},
                    {name: 'end_date',  type: 'string'},
                    {name: 'points_activity',  type: 'string'}
                    ]
           }
           });


