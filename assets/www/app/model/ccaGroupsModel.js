Ext.define('app.model.ccaGroupsModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'id', type: 'string' },
            { name: 'name', type: 'string' },
            { name: 'url_addr', type: 'string' },
			 { name: 'email_addr', type: 'string' }
            ]
    }
});


