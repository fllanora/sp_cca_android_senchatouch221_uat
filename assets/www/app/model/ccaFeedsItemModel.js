Ext.define('app.model.ccaFeedsItemModel', {
    extend: 'Ext.data.Model',

    config: {
       fields: [
		         {name: 'id', type: 'string'},
		         {name: 'title', type: 'string'},
		         {name: 'link', type:'string'},
		         {name: 'comments', type: 'string'},
		         {name: 'pubDate', type: 'Date'},
		         {name: 'description', type: 'string'}
		         ]
    }
});

