Ext.define('app.store.ccaFeedsPickerStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.ccaGroupsModel'
    ],

    config: {
        model: 'app.model.ccaGroupsModel',
     
	proxy: {
       type: 'ajax',
       url : 'http://cca.sg/spcca/sp-cca-groups.xml',
      //    url: 'data/sp-cca-groups.xml',
        reader: {
            type: 'xml',
		    root  : 'sp_cca_groups',
            record: 'organisation'
        }
        
    },
   autoLoad: true
	


    }
});