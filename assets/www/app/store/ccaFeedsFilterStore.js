Ext.define('app.store.ccaFeedsFilterStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.ccaFeedsItemModel'
    ],

    config: {
        model: 'app.model.ccaFeedsItemModel',
       autoLoad:false,
	proxy: {
        type: 'ajax',
        url : '',
        reader: {
            type: 'xml',
		    root  : 'channel',
            record: 'item'
        }
    }
	


    }
});